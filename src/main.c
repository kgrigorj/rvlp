#include <assert.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/eriks_freemem/freemem.h"
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "hmi.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include <time.h>
#include "../lib/helius_microrl/microrl.h"
#include "../lib/andygock_avr_uart/uart.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"


#define UART_BAUD 9600
#define UART_STATUS_MASK 0x00FF
#define LED_RED PORTA0 // Arduino Mega digital pin 22
#define LED_GREEN PORTA2 // Arduino Mega digital pin 24

// Create microrl object and pointer on it
microrl_t rl;
microrl_t *prl = &rl;

static inline void init_rfid_reader(void)
{
    /* Init RFID-RC522 */
    MFRC522_init();
    PCD_Init();
}

static inline void init_leds(void)
{
    DDRA |= _BV(LED_RED) | _BV(LED_GREEN);
    PORTA &= ~(_BV(LED_RED) | _BV(LED_GREEN));
}


static inline void init_uart1(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts_p(ER_CONSOLE_STARTED);
    uart1_puts_p(ver_fw);
    uart1_puts_p(ver_libc);
}


static inline void init_uart0(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(CONSOLE_STARTED);
    uart0_puts_p(stud_name);
    uart0_puts_p(ENTRY_MSG);
    uart0_puts_p(KEYS_MSG);
}


static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}


static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(LED_GREEN);
        prev_time = now;
    }
}


static inline void handle_rfid(void)
{
    Uid uid;
    Uid *uid_ptr = &uid;
    Uid prev_uid;
    uint8_t time_passed;
    time_t last_change;
    bool door_info_on_lcd;

    if (PICC_IsNewCardPresent()) {
        prev_uid = uid;
        PICC_ReadCardSerial(uid_ptr);
        //check if card is new
        bool same = true;

        if (time_passed > 3) {
            same = false;
        } else {
            if (uid.size != prev_uid.size) {
                same = false;
            } else {
                for (size_t i = 0; i < uid.size; i++) {
                    if (uid.uidByte[i] != prev_uid.uidByte[i]) {
                        same = false;
                        break;
                    }
                }
            }
        }

        //card is new, check the access
        if (!same) {
            rfid_card_t *c = card_ptr;
            bool has_access = false;

            while (c != NULL) {
                if (c->size == uid.size) {
                    bool same = true;

                    for (size_t i = 0; i < c->size; i++) {
                        if (uid.uidByte[i] != c->uid[i]) {
                            same = false;
                        }
                    }

                    if (same) { //match found
                        has_access = true;
                        last_change = time(NULL);
                        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                        lcd_goto(LCD_ROW_2_START);
                        lcd_puts(c->holder_name);
                        door_info_on_lcd = true;
                        PORTA |= _BV(LED_RED);
                        c = NULL;
                    } else { //check next card in list
                        c = c->next;
                    }
                }
            }

            if (!has_access) {
                last_change = time(NULL);
                lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                lcd_goto(LCD_ROW_2_START);
                lcd_puts_P(PSTR("Access denied!"));
                door_info_on_lcd = true;
                PORTA &= ~_BV(LED_RED);
            }
        }
    }

    time_passed = time(NULL) - last_change;

    //close the door after 3 seconds
    if (time_passed > 3) {
        PORTA &= ~_BV(LED_RED);
    }

    //clear the screen after 5 seconds
    if (time_passed > 5 && door_info_on_lcd) {
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        door_info_on_lcd = false;
    }
}


void main(void)
{
    sei();
    init_uart0();
    init_uart1();
    init_leds();
    lcd_init();
    lcd_home();
    lcd_puts_P(stud_name);
    init_sys_timer();
    init_rfid_reader();
    microrl_init(prl, uart0_puts);
    microrl_set_execute_callback(prl, cli_execute);

    while (1) {
        heartbeat();
        handle_rfid();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}


/* System timer ISR */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}

