//TODO There are most likely unnecessary includes. Clean up during lab6
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr_uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "hmi.h"
#include "cli_microrl.h"
#include "print_helper.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "../lib/matejx_avr_lib/spi.h"
#include "../lib/matejx_avr_lib/hwdefs.h"
#include "../lib/matejx_avr_lib/hwdefs_minimal.h"

#define NUM_ELEMS(x)    (sizeof(x) / sizeof((x)[0]))

void cli_print_help(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_banner(const char *const *argv);
void cli_handle_month(const char *const *argv);
void cli_rfid_read(const char *const *argv);
void cli_add_card(const char *const *argv);
void cli_print_cards(const char *const *argv);
void cli_remove_card(const char *const *argv);
void cli_mem_stat(const char *const *argv);
void print_bytes(void (*putc_function)(uint8_t data), const uint8_t *array,
                 const size_t len);

typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

rfid_card_t *card_ptr = NULL;

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";

const char banner_cmd[] PROGMEM = "banner";
const char banner_help[] PROGMEM = "Print banner";
const char month_cmd[] PROGMEM = "month";
const char month_help[] PROGMEM =
    "Print and display months by entered letters from beginning";
const char read_cmd[] PROGMEM = "read-uid";
const char read_help[] PROGMEM = "Read MIFARE card and print Card ID";
const char add_cmd[] PROGMEM = "add";
const char add_help[] PROGMEM =
    "Add MIFARE card to list. Usage: add <card uid in HEX> <card holder name>";
const char print_cmd[] PROGMEM = "print";
const char print_help[] PROGMEM = "Print stored access card list";
const char rm_cmd[] PROGMEM = "rm";
const char rm_help[] PROGMEM =
    "Remove MIFARE card from list Usage: rm <card uid in HEX>";
const char mem_cmd[] PROGMEM = "mem";
const char mem_help[] PROGMEM =
    "Print memory usage and change compared to previous call";


const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {banner_cmd, banner_help, cli_banner, 0},
    {month_cmd, month_help, cli_handle_month, 1},
    {read_cmd, read_help, cli_rfid_read, 0},
    {add_cmd, add_help, cli_add_card, 2},
    {print_cmd, print_help, cli_print_cards, 0},
    {rm_cmd, rm_help, cli_remove_card, 1},
    {mem_cmd, mem_help, cli_mem_stat, 0},

};


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}


void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}


void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(ver_fw);
    uart0_puts_p(ver_libc);
}


void cli_banner(const char *const *argv)
{
    (void) argv;
    print_banner_P(uart0_puts_p, banner, BANNER_ROWS);
}


void cli_handle_month(const char *const *argv)
{
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    for (uint8_t i = 0; i < NAME_MONTH_COUNT; i++) {
        if (!strncmp_P(argv[1], (PGM_P) pgm_read_word(&(name_month[i])),
                       strlen(argv[1]))) {
            uart0_puts_p((PGM_P) pgm_read_word(&(name_month[i])));
            uart0_puts_p(PSTR("\r\n"));
            lcd_puts_P((PGM_P) pgm_read_word(&(name_month[i])));
            lcd_putc(' ');
        }
    }
}


static inline uint8_t detect_card(void)
{
    uint8_t bufferATQA[2];
    uint8_t bufferSize = sizeof(bufferATQA);

    if (PICC_IsNewCardPresent()) {
        return 1;
    }

    if (PICC_WakeupA(bufferATQA, &bufferSize) == (STATUS_OK || STATUS_COLLISION)) {
        return 1;
    } else {
        return 0;
    }

    return 0;
}


void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;

    if (!detect_card()) {
        uart0_puts_p(PSTR("Unable to detect card.\r\n"));
        return;
    }

    uart0_puts_p(PSTR("Card selected!\r\n"));
    PICC_ReadCardSerial(&uid);
    uart0_puts_p(PSTR("Card type: "));
    uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
    uart0_puts_p(PSTR("\r\n"));
    uart0_puts_p(PSTR("Card UID: "));
    print_bytes(uart0_putc, uid.uidByte, uid.size);
    uart0_puts_p(PSTR(" (size "));
    print_bytes(uart0_putc, &uid.size, sizeof(uid.size));
    uart0_puts_p(PSTR(" bytes)\r\n"));
}


void cli_remove_card(const char *const *argv)
{
    rfid_card_t *c = card_ptr;
    rfid_card_t *last = NULL;
    size_t removed_card_uid_size = strlen(argv[1]) / 2;
    uint8_t *removed_card_uid = (uint8_t *)malloc(removed_card_uid_size * sizeof(
                                    uint8_t));
    tallymarker_hextobin(argv[1], removed_card_uid, removed_card_uid_size);

    if (card_ptr == NULL) {
        uart0_puts("No cards added yet.\r\n");
        return;
    } else {
        while (c != NULL) {
            bool found = true;

            for (size_t i = 0; i < removed_card_uid_size; i++) {
                if (c->uid[i] != removed_card_uid[i]) {
                    found = false;
                    break;
                }
            }

            if (found) {
                uart0_puts_p(PSTR("Card has been successfully removed!\r\n"));
                free(c->uid);
                free(c->holder_name);
                free(c);
                return;
            } else {
                if (last == NULL) {
                    card_ptr = c->next;
                } else {
                    last->next = c->next;
                }
            }

            last = c;
            c = c->next;
        }

        uart0_puts_p(PSTR("No card with this UID in list \r\n"));
    }
}


void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    char print_buf[256] = {0x00}; //FIXME reduce size if needed
    uint16_t space = getFreeMemory();
    static uint16_t prev_space;
    uart0_puts_p(PSTR("\r\nSpace between stack and heap:\r\n"));
    sprintf_P(print_buf, PSTR("Current  %d\r\nPrevious %d\r\nChange   %d\r\n"),
              space, prev_space, space - prev_space);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nHeap statistics\r\n"));
    sprintf_P(print_buf, PSTR("Used: %u\r\nFree: %u\r\n"), getMemoryUsed(),
              getFreeMemory());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest non freelist block:          %u\r\n"),
              getLargestNonFreeListBlock());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest dynamically allocable block: %u\r\n"),
              getLargestAvailableMemoryBlock());
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFreelist\r\n"));
    sprintf_P(print_buf, PSTR("Freelist size:             %u\r\n"),
              getFreeListSize());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Blocks in freelist:        %u\r\n"),
              getNumberOfBlocksInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest block in freelist: %u\r\n"),
              getLargestBlockInFreeList());
    uart0_puts(print_buf);
    prev_space = space; // Document space change for next call
}


void print_bytes(void (*putc_function)(uint8_t data), const uint8_t *array,
                 const size_t len)
{
    for (size_t i = 0; i < len; i++) {
        putc_function((array[i] >> 4) + ((array[i] >> 4) <= 9 ? 0x30 : 0x37));
        putc_function((array[i] & 0x0F) + ((array[i] & 0x0F) <= 9 ? 0x30 : 0x37));
    }
}


void cli_print_cards(const char *const *argv)
{
    (void) argv;
    rfid_card_t *c = card_ptr;
    int num = 1;
    char buffer[10];

    if (c == NULL) {
        uart0_puts_p(PSTR("Card list is empty \r\n"));
    }

    while (c != NULL) {
        uart0_puts_p(PSTR("Nr: "));
        uart0_puts(itoa(num, buffer, 10));
        num++;
        uart0_puts_p(PSTR("\r\nUID: "));
        print_bytes(uart0_putc, c->uid, c->size);
        uart0_puts_p(PSTR("\r\nUID size: "));
        uart0_puts(itoa(c->size, buffer, 10));
        uart0_puts_p(PSTR("\r\nHolder name: "));
        uart0_puts(c->holder_name);
        uart0_puts_p(PSTR("\r\n\r\n"));
        c = c->next;
    }
}


void cli_add_card(const char *const *argv)
{
    uart0_puts_p(PSTR("\r\n"));
    rfid_card_t *new_card;
    new_card = malloc(sizeof(rfid_card_t));

    if (!(strlen(argv[1]) > 20)) {
        if (new_card == NULL) {
            uart0_puts_p(PSTR("Failed to allocate memory\r\n"));
            free(new_card);
            return;
        } else {
            uart0_puts_p(
                PSTR("UID longer than 10 bytes not allowed!\r\n"));
            return;
        }
    }

    // Memory allocation and initializing new card values
    new_card->uid = malloc(strlen(argv[1]) + 1);

    if (new_card->uid == NULL) {
        uart0_puts_p(PSTR("Failed to allocate memory\r\n"));
        free(new_card);
        return;
    }

    new_card->size = strlen(argv[1]) / 2;
    tallymarker_hextobin(argv[1], new_card->uid, new_card->size);
    new_card->holder_name = malloc(strlen(argv[2]) + 1);

    if (new_card->holder_name == NULL) {
        uart0_puts_p(PSTR("Failed to allocate memory\r\n"));
        free(new_card);
        return;
    }

    strcpy(new_card->holder_name, argv[2]);

    // Check if card is already added
    if (card_ptr != NULL) {
        new_card->next = card_ptr;
        rfid_card_t *current = card_ptr;

        while (current != NULL) {
            if (!strcmp((char*)current->uid, (char*)new_card->uid)) {
                uart0_puts_p(PSTR("Card UID must be unique. Please use another card\r\n"));
                return;
            }

            current = current->next;
        }
    } else {
        new_card->next = NULL;
    }

    char uid_size[10];
    card_ptr = new_card;
    uart0_puts_p(PSTR("Added new card with UID: "));
    print_bytes(uart0_putc, new_card->uid, new_card->size);
    uart0_puts_p(PSTR("\r\nUID size: "));
    uart0_puts(itoa(new_card->size, uid_size, 10));
    uart0_puts_p(PSTR(" bytes\r\nHolder name: "));
    uart0_puts(new_card->holder_name);
    uart0_puts_p(PSTR("\r\n"));
}


int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    //FIXME Why microrl does not do it?
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                uart0_puts_p(
                    PSTR("To few or too many arguments for this command.\r\n\tUse <help>\r\n"));
                return 1;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p(argv);
            return 0;
        }
    }

    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
    return 1;
}
