#include <avr/pgmspace.h>
#include "hmi.h"
#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"
const char ver_fw[] PROGMEM =  VER_FW;
const char ver_libc[] PROGMEM = VER_LIBC;
const char stud_name[] PROGMEM = "Karen Grigorjan";
const char m1[] PROGMEM = "January";
const char m2[] PROGMEM = "February";
const char m3[] PROGMEM = "March";
const char m4[] PROGMEM = "April";
const char m5[] PROGMEM = "May";
const char m6[] PROGMEM = "June";
const char m7[] PROGMEM = "July";
const char m8[] PROGMEM = "August";
const char m9[] PROGMEM = "September";
const char m10[] PROGMEM = "October";
const char m11[] PROGMEM = "November";
const char m12[] PROGMEM = "December";
PGM_P const name_month[] PROGMEM = {
    m1,
    m2,
    m3,
    m4,
    m5,
    m6,
    m7,
    m8,
    m9,
    m10,
    m11,
    m12
};
const char line1[] PROGMEM = "(`-._o_.-')";
const char line2[] PROGMEM = " `--.|.--'";
const char line3[] PROGMEM = "   ( | )";
const char line4[] PROGMEM = "    (|)";
const char line5[] PROGMEM = "    (|)";
const char line6[] PROGMEM = "    '|`";
PGM_P const banner[] PROGMEM = {
    line1,
    line2,
    line3,
    line4,
    line5,
    line6
};
