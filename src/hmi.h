#include <avr/pgmspace.h>
#ifndef HMI_H
#define HMI_H
#define NAME_MONTH_COUNT 12
#define BANNER_ROWS 6
#define ENTER_MONTH "Enter Month name first letter >"
#define CONSOLE_STARTED "\r\nConsole Started\r\n"
#define ENTRY_MSG "\r\nUse backspace to delete entry and enter to confirm.\r\n"
#define ER_CONSOLE_STARTED "\r\nError Console started.\r\n"
#define KEYS_MSG "Arrow key's and del do not work currently.\r\n"

extern const char ver_fw[] PROGMEM;
extern const char ver_libc[] PROGMEM;
extern const char ver_fw[] PROGMEM;
extern const char ver_libc[] PROGMEM;
extern const char stud_name[] PROGMEM;
extern const char m1[] PROGMEM;
extern const char m2[] PROGMEM;
extern const char m3[] PROGMEM;
extern const char m4[] PROGMEM;
extern const char m5[] PROGMEM;
extern const char m6[] PROGMEM;
extern const char m7[] PROGMEM;
extern const char m8[] PROGMEM;
extern const char m9[] PROGMEM;
extern const char m10[] PROGMEM;
extern const char m11[] PROGMEM;
extern const char m12[] PROGMEM;
extern PGM_P const name_month[] PROGMEM;
extern const char line1[] PROGMEM;
extern const char line2[] PROGMEM;
extern const char line3[] PROGMEM;
extern const char line4[] PROGMEM;
extern const char line5[] PROGMEM;
extern const char line6[] PROGMEM;
extern PGM_P const banner[] PROGMEM;
#endif /* HMI_H */
