#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H
int cli_execute(int argc, const char *const *argv);
typedef struct rfid_card {
    uint8_t *uid;
    uint8_t size;
    char *holder_name;
    struct rfid_card *next;
} rfid_card_t;
extern rfid_card_t *card_ptr;
#endif /* CLI_MICRORL_H */
