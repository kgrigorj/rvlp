#ifndef PRINT_HELPER_H_
#define PRINT_HELPER_H_
#include <stdio.h>
void print_banner(void (*puts_P_function)(const char*), const char *const *banner, const uint8_t rows);
void print_banner_P(void (*puts_P_function)(const char*), const char * const *banner, const uint8_t rows);
uint8_t tallymarker_hextobin(const char * str, uint8_t * bytes, size_t blen);
#endif /* PRINT_HELPER_H_ */
